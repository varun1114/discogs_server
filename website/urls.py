from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('website.views',
    # Examples:
    # url(r'^$', 'discogs.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'index',name="index"),
    url(r'^addTag/$', 'addTag',name="addTag"),
    url(r'^upvoteTag/$', 'upvoteTag',name="upvoteTag"),
    url(r'^downvoteTag/$', 'downvoteTag',name="downvoteTag"),
    url(r'^login/$', 'login',name="login"),
    url(r'^search/$', 'search', name="search"),
    url(r'^playlists/$', 'getplaylists', name="getplaylists"),
    url(r'^recommend/$', 'recommend', name="recommend"),
    url(r'^searchall/$', 'searchall', name="searchall"),
    url(r'^getstyles/$', 'getstyles', name="getstyles"),
    url(r'^styleclick/$', 'styleclick', name="styleclick"),
    url(r'^logout/$', 'logout', name="logout"),
    url(r'^more/$', 'more', name="more"),
    url(r'^save/$', 'save', name="save"),
    url(r'^release/(?P<release_id>[0-9]+)/$', 'release', name="release"),
    url(r'^tag/(?P<tag_id>[0-9]+(,[0-9]+)*)/$', 'tag', name="tags"),
    url(r'^country/(?P<country>[a-zA-Z]+)/$', 'Country', name="Country"),
    url(r'^searchartist/(?P<artist>[a-zA-Z]+)/$', 'searchartist', name="searchartist"),
    url(r'^artist/(?P<artist_id>[0-9]+(,[0-9]+)*)/$', 'artist', name="artist"),
)
