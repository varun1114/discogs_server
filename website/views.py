from django.shortcuts import render
from django.template import RequestContext, loader
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import logout as auth_logout, login
from django.db import connection, transaction
from django.core import serializers
import json
import decimal
import re
from random import randint
import random
#from django.utils import simplejson


import MySQLdb as mdb

from social.backends.oauth import BaseOAuth1, BaseOAuth2
from social.backends.google import GooglePlusAuth
from social.backends.utils import load_backends
from social.apps.django_app.utils import psa
from operator import itemgetter

def index(request):
    return render(request, 'website/index.html')

def searchall(request):
    isJson = request.GET.get('json', '')
    query = request.GET['q']
    nextoffset = 0
    previousoffset = 0
    limit = 10
    try:
        nextoffset = int(request.GET['offset'])
    except Exception as e:
        nextoffset = 0
    try:
        limit = int(request.GET['limit'])
    except Exception as e:
        limit = 10
    #con = mdb.connect('50.74.234.98', 'discogs', 'discogs', 'discogs');
    cursor = connection.cursor()
    cursor.execute("select * from release where to_tsvector('english', title) @@ plainto_tsquery('english',%s) order by title limit %s offset %s", [query, limit, nextoffset])
    rows = cursor.fetchall()
    cursor.execute("select count(*) from release where to_tsvector('english', title) @@ plainto_tsquery('english',%s)", [query])
    row = cursor.fetchone()
    releases = []
    for r in rows:
        releases.append(r)
    #releases_json = json.dumps(releases)
    releases_json = releases
    if nextoffset+10 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 10
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 10
        nextoffset = nextoffset + 10
    c = {
            'query':query,
            'rows': rows,
            'offset':nextoffset,
            'previousoffset':previousoffset,
            'limit': limit,
        }
    all_info = json.dumps({'releases':releases_json,
                            'offset':nextoffset,
                            'previousoffset':previousoffset,
                            'limit':limit,
                            'query':query,
                            })
    if isJson == '':
        return render(request, 'website/search.html', c)
    else:
        return HttpResponse(all_info)

def search(request):
    isJson = request.GET.get('json', '')
    query = request.GET['q']
    nextoffset = 0
    previousoffset = 0
    try:
        nextoffset = int(request.GET['offset'])
    except Exception as e:
        nextoffset = 0
    #con = mdb.connect('50.74.234.98', 'discogs', 'discogs', 'discogs');
    cursor = connection.cursor()
    cursor.execute("select * from release where to_tsvector('english', title) @@ plainto_tsquery('english',%s) order by title limit 10 offset %s", [query , nextoffset])
    rows = cursor.fetchall()
    cursor.execute("select count(*) from release where to_tsvector('english', title) @@ plainto_tsquery('english',%s)", [query])
    row = cursor.fetchone()
    releases = []
    for r in rows:
        releases.append(r)
    #releases_json = json.dumps(releases)
    releases_json = releases
    if nextoffset+10 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 10
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 10
        nextoffset = nextoffset + 10
    c = {
            'query':query,
            'rows': rows,
            'offset':nextoffset,
            'previousoffset':previousoffset,
        }
    all_info = json.dumps({'releases':releases_json,
                            'offset':nextoffset,
                            'previousoffset':previousoffset,
                            'query':query,
                            })
    if isJson == '':
        return render(request, 'website/search.html', c)
    else:
        return HttpResponse(all_info)

def release(request, release_id):
    query = request.GET.get('json', '')
    #release_id = kwargs['pk']
    #print release_id
    cursor = connection.cursor()
    cursor.execute("select * from release where id = %s", [release_id])
    row1 = cursor.fetchall()
    release_json = row1

    cursor.execute("select * from releases_artists where release_id = %s", [release_id])
    row2 = cursor.fetchall()
    releases_artists = []
    for row in row2:
        releases_artists.append(row)
    #releases_artists_json = json.dumps(releases_artists)
    releases_artists_json = releases_artists

    cursor.execute("select * from releases_labels where release_id = %s", [release_id])
    row3 = cursor.fetchall()
    releases_labels = []
    for row in row3:
        #t = (row.label, row.release_id, row.catno)
        releases_labels.append(row)
    releases_labels_json = releases_labels

    cursor.execute("select * from releases_spotify where release_id = %s", [release_id])
    row4 = cursor.fetchall()
    releases_spotify_json = row4

    cursor.execute("select * from releases_youtube where release_id = %s", [release_id])
    row5 = cursor.fetchall()
    releases_youtube_json = row5

    cursor.execute("select * from track where release_id = %s order by trackno", [release_id])
    tracks = cursor.fetchall()
    track = []
    for row in tracks:
        #t = (row.position, row.track_id, row.title, row.duration, row.trackno)
        track.append(row)
    track_json = track

    cursor.execute("select id, tag, r.likes as likes, r.dislikes as dislikes from tags_releases r join tags_names t on tag_id = id where release_id = %s", [release_id])
    tags = cursor.fetchall()
    tag = []
    for row in tags:
        #t = (row.id, row.tag, row.likes, row.dislikes)
        tag.append(row)
    tag_json = tag


    for tag in tags:
        print tag[1]

    c = {
            'query' : release_id,
            'row1' : row1,
            'row2' : row2,
            'row3' : row3,
            'row4' : row4,
            'row5' : row5,
            'tags' : tags,
            'tracks' : tracks,
        }
    all_info = json.dumps( {'release':release_json,
                        'releases_artists':releases_artists_json,
                        'releases_labels':releases_labels_json,
                        'releases_spotify': releases_spotify_json,
                        'releases_youtube':releases_youtube_json,
                        'tag':tag_json,
                        'track':track_json})
    if query == '':
        return render(request, 'website/release.html', c)
    else:
        return HttpResponse(all_info)

def tag(request, tag_id):
    query = request.GET.get('json', '')
    if query == '':
        nextoffset = 0
        previousoffset = 0
        try:
            nextoffset = int(request.GET['offset'])
        except Exception as e:
            nextoffset = 0
        cursor = connection.cursor()
        ids = tag_id.split(",")
        query = "(SELECT release_id FROM tags_releases WHERE tag_id = " + ids.pop(0) + ")"
        for id in ids:
            query += " intersect (SELECT release_id FROM tags_releases WHERE tag_id = " + id + ")"
        query += " order by release_id limit 10 offset " + str(nextoffset)
        cursor.execute(query);
        row2 = cursor.fetchall()
        row1 = []
        print len(row2)
        ids = []
        for row in row2:
            ids.append(str(row[0]))
        format_strings = ','.join(['%s'] * len(ids))
        cursor.execute("select * from release where id in (%s)" % format_strings, tuple(ids))
        row1 = (cursor.fetchall())
        if nextoffset+10 >= row[0]:
            if nextoffset == 0:
                previousoffset = nextoffset
                nextoffset = nextoffset
            else:
                previousoffset = nextoffset - 10
                nextoffset = nextoffset
        else:
            if nextoffset == 0:
                previousoffset = 0
            else:
                previousoffset = nextoffset - 10
            nextoffset = nextoffset + 10

        c = {
                'query' : tag_id,
                'row1' : row1,
                'offset':nextoffset,
                'previousoffset':previousoffset,
            }
        return render(request, 'website/tags.html', c)
    else:
        cursor = connection.cursor()
        query = "select * from (select distinct on (c.title) c.title, a.release_id, sum(upvotes)/sum(views) as rating from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0 group by a.release_id, c.title) d order by rating desc limit 100"
        #query = "select * from (select distinct on (c.title) c.title, a.release_id, sum(views)*.5 + (sum(upvotes)/(sum(upvotes) + sum(downvotes)+ 1))*.5 as rating from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0 group by a.release_id, c.title) d order by rating desc limit 100"
        #query = "select * from (select distinct on (c.title) c.title, a.release_id, views from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0) d order by views desc limit 100"
        #query = "select c.title, a.release_id, views from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0 order by views desc limit 100"
        cursor.execute(query);
        rows = cursor.fetchall()
        track = []
        for row in rows:
            #t = (row.position, row.track_id, row.title, row.duration, row.trackno)
            track.append(row)
            #track.append([str(i) for i in row])
        all_info = json.dumps( {'tracks':track})
        return HttpResponse(all_info)

class DecimalEncoder(json.JSONEncoder):
    def _iterencode(self, o, markers=None):
        if isinstance(o, decimal.Decimal):
            # wanted a simple yield str(o) in the next line,
            # but that would mean a yield on the line with super(...),
            # which wouldn't work (see my comment below), so...
            return (str(o) for o in [o])
        return super(DecimalEncoder, self)._iterencode(o, markers)



def Country(request, country):
    nextoffset = 0
    previousoffset = 0
    try:
        nextoffset = int(request.GET['offset'])
    except Exception as e:
        nextoffset = 0
    cursor = connection.cursor()

    cursor.execute("select * from release where to_tsvector('english', country) @@ plainto_tsquery('english',%s) order by country limit 10 offset %s", [country, nextoffset])
    rows = cursor.fetchall()
    cursor.execute("select count(*) from release where to_tsvector('english', country) @@ plainto_tsquery('english',%s)", [country])
    row = cursor.fetchone()

    if nextoffset+10 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 10
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 10
        nextoffset = nextoffset + 10

    c = {
            'query' : country,
            'rows' : rows,
            'offset':nextoffset,
            'previousoffset':previousoffset,
        }
    return render(request, 'website/country.html', c)


def searchartist(request, artist):
    nextoffset = 0
    previousoffset = 0
    try:
        nextoffset = int(request.GET['offset'])
    except Exception as e:
        nextoffset = 0
    cursor = connection.cursor()

    cursor.execute("select release_id from releases_artists where to_tsvector('english', artist_name) @@ plainto_tsquery('english',%s) limit 10 offset %s", [artist, nextoffset])
    rows1 = cursor.fetchall()

    ids = []
    for row in rows1:
        ids.append(str(row[0]))

    for id in ids:
        id = id.encode('utf-8')
    format_strings = ','.join(['%s'] * len(ids))

    ids.append(nextoffset)

    query = 'select * from release where id in (%s)' % format_strings + ' limit 10 offset %s', tuple(ids)
    cursor.execute('select * from release where id in (%s)' % format_strings + ' limit 10 offset %s', tuple(ids))
    rows = cursor.fetchall()
    if nextoffset+10 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 10
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 10
        nextoffset = nextoffset + 10

    c = {
            'query' : artist,
            'rows' : rows,
            'offset':nextoffset,
            'previousoffset':previousoffset,
        }
    return render(request, 'website/country.html', c)




def artist(request, artist_id):
    nextoffset = 0
    previousoffset = 0
    try:
        nextoffset = int(request.GET['offset'])
    except Exception as e:
        nextoffset = 0
    cursor = connection.cursor()

    cursor.execute("select release_id from releases_artists where artist_id = %s limit 10 offset %s", [artist_id, nextoffset])
    rows1 = cursor.fetchall()

    ids = []
    for row in rows1:
        ids.append(str(row[0]))

    for id in ids:
        id = id.encode('utf-8')
    format_strings = ','.join(['%s'] * len(ids))

    ids.append(nextoffset)

    query = 'select * from release where id in (%s)' % format_strings + ' limit 10 offset %s', tuple(ids)
    cursor.execute('select * from release where id in (%s)' % format_strings + ' limit 10 offset %s', tuple(ids))
    rows = cursor.fetchall()
    if nextoffset+10 >= row[0]:
        if nextoffset == 0:
            previousoffset = nextoffset
            nextoffset = nextoffset
        else:
            previousoffset = nextoffset - 10
            nextoffset = nextoffset
    else:
        if nextoffset == 0:
            previousoffset = 0
        else:
            previousoffset = nextoffset - 10
        nextoffset = nextoffset + 10

    c = {
            'query' : artist,
            'rows' : rows,
            'offset':nextoffset,
            'previousoffset':previousoffset,
        }
    return render(request, 'website/country.html', c)


def login(request):
   	context = RequestContext(request,
                           {'request': request,
                            'user': request.user})
   	return render_to_response('website/index.html',
                            context_instance=context)

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/home/')

#action
#1 - Create
#2 - Upvote
#3- Downvote
def addTag(request):
    tag = request.GET['tag']
    release = request.GET['release']
    cursor = connection.cursor()
    cursor.execute("select count(*) from tags_names where tag = %s ", [tag])
    exists = cursor.fetchone()[0]
    if exists == 0:
        cursor.execute("insert into tags_names (tag, likes, dislikes, clicks) values (%s, %s, %s, %s) ", [tag, 0, 0, 0])
    cursor.execute("select id from tags_names where tag = %s ", [tag])
    tagid = cursor.fetchone()[0]
    cursor.execute("select count(*) from tags_releases where tag_id = %s ", [tagid])
    exists = cursor.fetchone()[0]
    if exists == 0:
        cursor.execute("insert into tags_releases (release_id, tag_id , likes, dislikes, clicks) values (%s,%s, %s, %s, %s) ", [release, tagid, 0, 0, 0])
    cursor.execute("select count(*) from tags_releases where tag_id = %s ", [tagid])
    exists = cursor.fetchone()[0]
    if exists == 0:
        cursor.execute("insert into tags_releases (tag_id , release_id) values (%s,%s) ", [tagid, release])

    print request.user.id
    cursor.execute("insert into tags_users (user_id, tag_id, release_id, action) values (%s, %s, %s, %s) ", [request.user.id, tagid, release, 1])
    cursor.commit()
    return HttpResponse(tagid)

def upvoteTag(request):
    tagid = request.GET['tagid']
    release = request.GET['release']
    cursor = connection.cursor()
    cursor.execute("select * from tags_users where user_id = %s and tag_id = %s and release_id = %s", [request.user.id, tagid, release])
    exists = cursor.fetchone()
    # if not exists create and insert into user, tag, release_tag
    if exists == None:
        print "didnt exist"
        cursor.execute("insert into tags_users (user_id, tag_id, release_id, action) values (%s, %s, %s, %s) ", [request.user.id, tagid, release, 2])
        cursor.execute("update tags_releases set likes = likes + 1 where tag_id = %s and release_id = %s", [tagid, release])
        cursor.execute("update tags_names set likes = likes + 1 where id = %s", [tagid])
        return HttpResponse("Upvoted!")
    else:
        #if created
        if exists[3] == 1:
            print "created"
            return HttpResponse("Cannot upvote created tag")
        # remove upvote
        elif exists[3] == 2:
            print "upvoted"
            cursor.execute("delete from tags_users where user_id = %s and tag_id = %s and release_id = %s", [request.user.id, tagid, release])
            cursor.execute("update tags_releases set likes = likes - 1 where tag_id = %s and release_id = %s", [tagid, release])
            cursor.execute("update tags_names set likes = likes - 1 where id = %s", [tagid])
            return HttpResponse("Removed Upvote")
        elif exists[3] == 3:
            print "downvoted"
            cursor.execute("update tags_users set action = 2 where user_id = %s and tag_id = %s and release_id = %s", [request.user.id, tagid, release])
            cursor.execute("update tags_releases set likes = likes + 1, dislikes = dislikes - 1 where tag_id = %s and release_id = %s", [tagid, release])
            cursor.execute("update tags_names set likes = likes + 1, dislikes = dislikes - 1 where id = %s", [tagid])
            return HttpResponse("Upvoted!")

    #print request.user.id
    cursor.commit()

    return HttpResponse("Upvoted!")

def downvoteTag(request):
    tagid = request.GET['tagid']
    release = request.GET['release']
    cursor = connection.cursor()
    cursor.execute("select * from tags_users where user_id = %s and tag_id = %s and release_id = %s", [request.user.id, tagid, release])
    exists = cursor.fetchone()
    # if not exists create and insert into user, tag, release_tag
    if exists == None:
        print "didnt exist"
        cursor.execute("insert into tags_users (user_id, tag_id, release_id, action) values (%s, %s, %s, %s) ", [request.user.id, tagid, release, 3])
        cursor.execute("update tags_releases set dislikes = dislikes + 1 where tag_id = %s and release_id = %s", [tagid, release])
        cursor.execute("update tags_names set dislikes = dislikes + 1 where id = %s", [tagid])
        return HttpResponse("Downvoted!")
    else:
        if exists[3] == 1:
            print "created"
            return HttpResponse("Cannot downvote created tag")
        elif exists[3] == 2:
            print "upvoted"
            cursor.execute("update tags_users set action = 3 where user_id = %s and tag_id = %s and release_id = %s", [request.user.id, tagid, release])
            cursor.execute("update tags_releases set dislikes = dislikes + 1, likes = likes - 1 where tag_id = %s and release_id = %s", [tagid, release])
            cursor.execute("update tags_names set dislikes = dislikes + 1, likes = likes - 1 where id = %s", [tagid])
            return HttpResponse("Downvoted!")
        elif exists[3] == 3:
            print "downvoted"
            cursor.execute("delete from tags_users where user_id = %s and tag_id = %s and release_id = %s", [request.user.id, tagid, release])
            cursor.execute("update tags_releases set dislikes = dislikes - 1 where tag_id = %s and release_id = %s", [tagid, release])
            cursor.execute("update tags_names set dislikes = dislikes - 1 where id = %s", [tagid])
            return HttpResponse("Cannot downvote, downvoted tag")

    #print request.user.id
    cursor.commit()

    return HttpResponse("Downvoted!")

def more(request):
    artist = ") | (".join("&".join(request.GET.get('artist', 'TRUE').split(" ")).split(","))
    if artist != "TRUE":
        artist = "to_tsvector('english', artist_name) @@ to_tsquery('" + "(" + artist + ")" + "')"
    #style = "%|%".join("&".join(request.GET.get('style', 'TRUE').split(" ")).split(","))
    style = ") | (".join("&".join(request.GET.get('style', 'TRUE').split(" ")).split(","))
    if style != "TRUE":
        style = "to_tsvector('english', styles) @@ to_tsquery('" + "(" + style + ")" + "')"
        #style = "styles similar to '" + "(%" + style + "%)'"
        #style = "(" + style + ")"
    genre = ") | (".join("&".join(request.GET.get('genre', 'TRUE').split(" ")).split(","))
    if genre != "TRUE":
        genre = "to_tsvector('english', genres) @@ to_tsquery('" + "(" + genre + ")" + "')"
        #genre = "(" + genre + ")"
    time = ") | (".join("&".join(request.GET.get('time', 'TRUE').split(" ")).split(","))
    if time != "TRUE":
        time = "to_tsvector('english', released) @@ to_tsquery('" + "(" + time + ")" + "')"
        #time = "(" + time + ")"
    country = ") | (".join("&".join(request.GET.get('country', 'TRUE').split(" ")).split(","))
    if country != "TRUE":
        country = "to_tsvector('english', country) @@ to_tsquery('" + "(" + country + ")" + "')"
        #country = "(" + country + ")"
    release_id = request.GET.get('release_id', '')
    cursor = connection.cursor()
    print style

    #query = "select count(*) from release r join releases_artists a on r.id = a.release_id join releases_youtube y on a.release_id = y.release_id where " + artist + " and " + style + " and " + genre + " and " + country + " and " + time + " and a.release_id <> " + release_id + " and views <> 0 and upvotes <> 0"
    #cursor.execute(query)
    #count = cursor.fetchone()[0]
#    random = randint(0,count - 1)
    #query = "select r.id as rid, views from release r join releases_artists a on r.id = a.release_id join releases_youtube y on a.release_id = y.release_id where " + artist + " and " + style + " and " + genre + " and " + country + " and " + time + " and a.release_id <> " + release_id + " and views <> 0 and upvotes <> 0 order by RANDOM() limit 100"
    query = "select r.id as rid, views from release r join releases_artists a on r.id = a.release_id join releases_youtube y on a.release_id = y.release_id where " + artist + " and " + style + " and " + genre + " and " + country + " and " + time + " and a.release_id <> " + release_id + " order by RANDOM() limit 100"

    print query
    cursor.execute(query)
    #cursor.execute("select r.id from release r join releases_artists a on r.id = a.release_id join releases_youtube y on a.release_id = y.release_id where %s and %s and %s and %s and %s and a.release_id <> %s ORDER BY RANDOM() limit 100", [artist,style, genre,country, time , release_id])
    #query = "select * from (select distinct on (c.title) c.title, a.release_id, sum(views)*.5 + (sum(upvotes)/(sum(upvotes) + sum(downvotes)+ 1))*.5 as rating from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0 group by a.release_id, c.title) d order by rating desc limit 100"
    #query = "select * from (select distinct on (c.title) c.title, a.release_id, views from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0) d order by views desc limit 100"
    #query = "select c.title, a.release_id, views from tags_releases a join releases_youtube b on a.release_id = b.release_id join release c on id = b.release_id where tag_id = " + tag_id + " and views > 0 order by views desc limit 100"
    #cursor.execute(query);
    rows = cursor.fetchall()
    track = []
    for row in rows:
        #t = (row.position, row.track_id, row.title, row.duration, row.trackno)
        track.append(row)
        #track.append([str(i) for i in row])
    all_info = json.dumps( {'tracks':track})
    return HttpResponse(all_info)


def getplaylists(request):
    userid = 0
    try:
        userid = int(request.GET['userid'])
    except Exception as e:
        userid = 0
    cursor = connection.cursor()

    cursor.execute("select * from users_playlists where user_id = %s", [userid])
    rows = cursor.fetchall()
    playlists = []
    for r in rows:
        playlists.append(r)
    all_info = json.dumps({'playlists':playlists,
                            })

    return HttpResponse(all_info)

def getstyles(request):
    releaseid = 0
    try:
        releaseid = int(request.GET['releaseid'])
    except Exception as e:
        releaseid = 0
    cursor = connection.cursor()

    cursor.execute("select styles, genres from release where id = %s", [releaseid])
    rows = cursor.fetchall()
    styles = []
    genres = []
    for row in rows:
        temp = re.sub('[^A-Za-z0-9\s\,]+', '', row[0])
        styles = temp.split(',')
        temp = re.sub('[^A-Za-z0-9\s\,]+', '', row[1])
        temp = temp.replace (" ", " & ")
        print temp
        genres = temp.split(',')
    '''for g in genres:
        print g
        if " " in g:
            g = g.replace(" ", "&")
    '''
    print genres
    genre = ") | (".join(genres)
    randomInt = random.randint(1,5624375)
    genre = "to_tsvector('english', genres) @@ to_tsquery('" + "(" + genre + ")" + "')"
    query = "select styles, genres from (select r.id as rid, r.title as title, r.released as year, r.country as country, views, r.styles, r.genres from release r join releases_artists a on r.id = a.release_id join releases_youtube y on a.release_id = y.release_id where " + genre + " order by rid) as a where a.rid>random()*" + str(randomInt) + " limit 1"
    print query
    cursor.execute(query)
    rows = cursor.fetchall()
    newStyle = [] 
    for row in rows:
        temp = re.sub('[^A-Za-z0-9\s\,]+', '', row[0])
        print "temp: " + temp                    
        newStyle = temp.split(',')
        print newStyle
        random.shuffle(newStyle)
        #newStyle = newStyle[:2]
    print newStyle
    styles = styles + newStyle    
    all_info = json.dumps({'options':styles,
                            })

    return HttpResponse(all_info)

def styleclick(request):
    userid = 0
    releaseid = 0
    tag = ""
    try:
        userid = int(request.GET['userid'])
        releaseid = int(request.GET['releaseid'])
        tag = request.GET['tag']
    except Exception as e:
        userid = 0
        releaseid = 0
    
    cursor = connection.cursor()
    query = "insert into users_tags (tag, release_id, user_id, date) values (" + tag + ", " + str(releaseid) + ", " + str(userid) +  ", CURRENT_TIMESTAMP)"
    print query
    cursor.execute(query)
    return HttpResponse(query)    
    
def recommend(request):
    userid = 0
    try:
        userid = int(request.GET['userid'])
    except Exception as e:
        userid = 0
    cursor = connection.cursor()
    cursor1 = connection.cursor()
    cursor.execute("select * from users_playlists where user_id = %s", [userid])
    rows = cursor.fetchall()
    playlists = []
    reco_styles = {}
    reco_artists = {}
    maxTag = ""
    for r in rows:
        playlists.append(r)
        #print r[1]
        cursor.execute("select * from playlists where playlist_id = %s", [r[1]])
        rows1 = cursor.fetchall()
        for r1 in rows1:
            print r1[1]
            cursor1.execute("select styles from release where id = %s", [r1[1]])
            rows2 = cursor1.fetchall()
            for r2 in rows2:
                print r2[0]
                styles = r2[0].split(',')
                for style in styles:
                    st = style
                    st = re.sub('[^A-Za-z0-9\s]+', '', st)
                    st1 = st.split(" ")
                    for s in st1:
                        if reco_styles.has_key(s):
                            reco_styles[s] = reco_styles[s] + 1
                            #if reco_styles.has_key(maxTag) and int(reco_styles[maxTag]) < int(reco_styles[s]):
                                #maxTag = s
                        else:
                            reco_styles[s] = 1
                            #if not reco_styles.has_key(maxTag):
                                #maxTag = s

            '''cursor1.execute("select artist_id from releases_artists where release_id = %s", [r1[1]])
            rows2 = cursor1.fetchall()
            for s in rows2:
                if reco_artists.has_key(s):
                    reco_artists[s] = reco_artists[s] + 1
                else:
                    reco_artists[s] = 1
            '''
    #reco_s = sorted(reco_styles.items(), key=lambda x: x[1])
    reco_s = sorted(reco_styles, key=reco_styles.get)
    #style = ") | (".join("&".join(request.GET.get('style', 'TRUE').split(" ")).split(","))
    reco_s = reco_s[-3:]
    style = ") | (".join(reco_s)
    #style = style[-2:]
    print style
    style = "to_tsvector('english', styles) @@ to_tsquery('" + "(" + style + ")" + "')"
    query = "select * from (select r.id as rid, r.title as title, r.released as year, r.country as country, views from release r join releases_artists a on r.id = a.release_id join releases_youtube y on a.release_id = y.release_id where " + style + " order by rid) as a where a.rid>random()*5624375 limit 1000"
    print query
    cursor.execute(query)
    rows = cursor.fetchall()
    track = []
    for row in rows:
        track.append(row)
    all_info = json.dumps( {'tracks':track})
    return HttpResponse(all_info)

def save(request):
    releases = request.GET.get('releases', '').split(",")
    youtubes = request.GET.get('youtubes', '').split(",")
    userid = request.GET.get('userid', '')
    cursor = connection.cursor()
    query = "insert into users_playlists (user_id, date) values (" + userid + ", CURRENT_TIMESTAMP) RETURNING playlist_id"

    print query
    cursor.execute(query)
    playlist_id  = cursor.fetchone()[0]
    for i in range(0, len(releases)):
        query = "insert into playlists (playlist_id, release_id, youtube_id) values (" + str(playlist_id) + ", " + releases[i] + ", '" + youtubes[i] + "')"
        cursor.execute(query)
        print query
    #track = []
    #for row in rows:
        ##t = (row.position, row.track_id, row.title, row.duration, row.trackno)
        #track.append(row)
        ##track.append([str(i) for i in row])
    #all_info = json.dumps( {'tracks':track})
    return HttpResponse(query)
