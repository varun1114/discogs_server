# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Artist(models.Model):
    id = models.IntegerField()
    name = models.TextField()
    realname = models.TextField(blank=True)
    urls = models.TextField(blank=True)  # This field type is a guess.
    namevariations = models.TextField(blank=True)  # This field type is a guess.
    aliases = models.TextField(blank=True)  # This field type is a guess.
    releases = models.TextField(blank=True)  # This field type is a guess.
    profile = models.TextField(blank=True)
    members = models.TextField(blank=True)  # This field type is a guess.
    groups = models.TextField(blank=True)  # This field type is a guess.
    data_quality = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'artist'


class ArtistsImages(models.Model):
    image_uri = models.ForeignKey('Image', db_column='image_uri')
    type = models.TextField()
    artist_id = models.IntegerField()
    id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'artists_images'


class AuthGroup(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    group = models.ForeignKey(AuthGroup)
    permission = models.ForeignKey('AuthPermission')

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'


class AuthPermission(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=50)
    content_type = models.ForeignKey('DjangoContentType')
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'


class AuthUser(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField()
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=30)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=75)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser)
    group = models.ForeignKey(AuthGroup)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'


class AuthUserUserPermissions(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    user = models.ForeignKey(AuthUser)
    permission = models.ForeignKey(AuthPermission)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'


class Country(models.Model):
    name = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'country'


class DjangoAdminLog(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', blank=True, null=True)
    user = models.ForeignKey(AuthUser)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.CharField(max_length=100)
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'


class DjangoMigrations(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class Format(models.Model):
    name = models.TextField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'format'


class Genre(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField(blank=True)
    parent_genre = models.IntegerField(blank=True, null=True)
    sub_genre = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'genre'


class Image(models.Model):
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    uri = models.TextField(primary_key=True)
    uri150 = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'image'


class Label(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    name = models.TextField()
    contactinfo = models.TextField(blank=True)
    profile = models.TextField(blank=True)
    parent_label = models.TextField(blank=True)
    sublabels = models.TextField(blank=True)  # This field type is a guess.
    urls = models.TextField(blank=True)  # This field type is a guess.
    data_quality = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'label'


class LabelsImages(models.Model):
    image_uri = models.ForeignKey(Image, db_column='image_uri')
    type = models.TextField()
    label = models.ForeignKey(Label)

    class Meta:
        managed = False
        db_table = 'labels_images'


class Master(models.Model):
    id = models.IntegerField()
    title = models.TextField(blank=True)
    main_release = models.IntegerField()
    year = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True)
    genres = models.TextField(blank=True)
    styles = models.TextField(blank=True)
    data_quality = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'master'


class MastersArtists(models.Model):
    artist_name = models.TextField(blank=True)
    master_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'masters_artists'


class MastersArtistsJoins(models.Model):
    artist1 = models.TextField(blank=True)
    artist2 = models.TextField(blank=True)
    join_relation = models.TextField(blank=True)
    master_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'masters_artists_joins'


class MastersExtraartists(models.Model):
    master_id = models.IntegerField(blank=True, null=True)
    artist_name = models.TextField(blank=True)
    roles = models.TextField(blank=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'masters_extraartists'


class MastersFormats(models.Model):
    master_id = models.IntegerField(blank=True, null=True)
    format_name = models.TextField(blank=True)
    qty = models.IntegerField(blank=True, null=True)
    descriptions = models.TextField(blank=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'masters_formats'


class MastersImages(models.Model):
    image_uri = models.ForeignKey(Image, db_column='image_uri')
    type = models.TextField()
    master_id = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'masters_images'


class Release(models.Model):
    id = models.IntegerField(primary_key=True)  # AutoField?
    status = models.TextField(blank=True)
    title = models.TextField(blank=True)
    country = models.TextField(blank=True)
    released = models.TextField(blank=True)
    barcode = models.TextField(blank=True)
    notes = models.TextField(blank=True)
    genres = models.TextField(blank=True)
    styles = models.TextField(blank=True)
    master_id = models.IntegerField(blank=True, null=True)
    data_quality = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'release'


class ReleasesArtists(models.Model):
    release_id = models.IntegerField()
    position = models.IntegerField()
    artist_id = models.IntegerField(blank=True, null=True)
    artist_name = models.TextField(blank=True)
    anv = models.TextField(blank=True)
    join_relation = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'releases_artists'


class ReleasesExtraartists(models.Model):
    release_id = models.IntegerField(blank=True, null=True)
    artist_id = models.IntegerField(blank=True, null=True)
    artist_name = models.TextField(blank=True)
    anv = models.TextField(blank=True)
    role = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'releases_extraartists'


class ReleasesFormats(models.Model):
    release = models.ForeignKey(Release)
    position = models.IntegerField()
    format_name = models.ForeignKey(Format, db_column='format_name', blank=True, null=True)
    qty = models.IntegerField(blank=True, null=True)
    descriptions = models.TextField(blank=True)  # This field type is a guess.

    class Meta:
        managed = False
        db_table = 'releases_formats'


class ReleasesImages(models.Model):
    image_uri = models.ForeignKey(Image, db_column='image_uri')
    type = models.TextField()
    release = models.ForeignKey(Release)

    class Meta:
        managed = False
        db_table = 'releases_images'


class ReleasesLabels(models.Model):
    label = models.TextField(blank=True)
    release = models.ForeignKey(Release, blank=True, null=True)
    catno = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'releases_labels'


class ReleasesSpotify(models.Model):
    release_id = models.IntegerField(primary_key=True)
    artist_id = models.IntegerField(blank=True, null=True)
    spotify_album_id = models.TextField(blank=True)
    popularity = models.IntegerField(blank=True, null=True)
    spotify_song_id = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'releases_spotify'


class ReleasesYoutube(models.Model):
    release_id = models.IntegerField(primary_key=True)
    artist_id = models.IntegerField(blank=True, null=True)
    youtube_id = models.TextField(blank=True)
    title = models.TextField(blank=True)
    description = models.TextField(blank=True)
    duration = models.IntegerField(blank=True, null=True)
    views = models.IntegerField(blank=True, null=True)
    rating = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'releases_youtube'


class Role(models.Model):
    role_name = models.TextField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'role'


class TmpArtistsImages(models.Model):
    image_uri = models.TextField(blank=True)
    type = models.TextField(blank=True)
    artist_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_artists_images'


class TmpImage(models.Model):
    height = models.IntegerField(blank=True, null=True)
    width = models.IntegerField(blank=True, null=True)
    uri = models.TextField()
    uri150 = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'tmp_image'


class TmpLabelsImages(models.Model):
    image_uri = models.TextField(blank=True)
    type = models.TextField(blank=True)
    label_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_labels_images'


class TmpMastersImages(models.Model):
    image_uri = models.TextField(blank=True)
    type = models.TextField(blank=True)
    master_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_masters_images'


class TmpReleasesImages(models.Model):
    image_uri = models.TextField(blank=True)
    type = models.TextField(blank=True)
    release_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tmp_releases_images'


class Track(models.Model):
    release_id = models.IntegerField(blank=True, null=True)
    position = models.TextField(blank=True)
    track_id = models.TextField(primary_key=True)
    title = models.TextField(blank=True)
    duration = models.TextField(blank=True)
    trackno = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'track'


class TracksArtists(models.Model):
    track_id = models.TextField()
    position = models.IntegerField()
    artist_id = models.IntegerField(blank=True, null=True)
    artist_name = models.TextField(blank=True)
    anv = models.TextField(blank=True)
    join_relation = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'tracks_artists'


class TracksExtraartists(models.Model):
    track_id = models.TextField(blank=True)
    artist_id = models.IntegerField(blank=True, null=True)
    artist_name = models.TextField(blank=True)
    anv = models.TextField(blank=True)
    role = models.TextField(blank=True)

    class Meta:
        managed = False
        db_table = 'tracks_extraartists'
